# BanglaDictX: A cross-platform English to Bengali dictionary #

### How do I get set up? ###

* Just make sure that both the /data and /icon directories are inside the /src folder
* Create a runnable jar from the Export menu of Eclipse
* Run the command: java -jar BanglaDictX.jar (Assuming that you've created the jar file of same name)