import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class AppLauncher {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
			try {
		        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		    } 
		    catch (UnsupportedLookAndFeelException e) {
		    }
		    catch (ClassNotFoundException e) {
		    }
		    catch (InstantiationException e) {
		    }
		    catch (IllegalAccessException e) {
		    }
				new MainWindow();
			}
		});
	}

}
