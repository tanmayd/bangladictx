import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class MainWindow extends JFrame implements ActionListener{
	private JPanel leftPanel, rightPanel, innerLeft, aboutPanel;
	private JButton button, addToFav, clearButton, okButtonAbout;
	private JLabel imgLabel, searchLabel, favLabel, appTitleAbout;
	private JTextField searchBox;
	private JList<String> wordList;
	private JMenuBar menuBar;
	private JMenu fileMenu, helpMenu;
	private JMenuItem addFavItem, clearFavItem, exitDictItem, aboutItem;
	private JDialog aboutDialog;
	File file = new File("words.txt");
	DefaultListModel<String> listModel;
	public MainWindow(){
		super("BanglaDictX: A cross-platform En-Bn dictionary");
		
		leftPanel = new JPanel();
		rightPanel = new JPanel();
		innerLeft = new JPanel(new GridBagLayout());
		aboutPanel = new JPanel();
		button = new JButton("Go");
		addToFav = new JButton("Add To Favorites");
		clearButton = new JButton("Clear Favorites");
		okButtonAbout = new JButton("Ok");
		imgLabel = new JLabel();
		searchLabel = new JLabel("Search: ");
		favLabel = new JLabel("Favorites:");
		appTitleAbout = new JLabel("BanglaDictX - v1.0");
		searchBox = new JTextField(12);
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		helpMenu = new JMenu("Help");
		addFavItem = new JMenuItem("Add To Favorites");
		clearFavItem = new JMenuItem("Clear Favorites");
		exitDictItem = new JMenuItem("Exit");
		aboutItem = new JMenuItem("About");
		wordList = new JList<String>();
		listModel = new DefaultListModel<String>();
		aboutDialog = new JDialog(this,"About",true);
		aboutDialog.setSize(220, 150);
		aboutDialog.setLocationRelativeTo(null);
		
		setJMenuBar(menuBar);
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		fileMenu.add(addFavItem);
		fileMenu.add(clearFavItem);
		fileMenu.addSeparator();
		fileMenu.add(exitDictItem);
		helpMenu.add(aboutItem);
		
		aboutDialog.add(aboutPanel);
		aboutDialog.setResizable(false);

		aboutPanel.add(appTitleAbout);
		aboutPanel.add(new JLabel("A Cross-Platform En-Bn Dictionary"));
		aboutPanel.add(new JLabel("(C) Tanmay Das, 2015"));
		aboutPanel.add(new JLabel("E-mail : tanmaymishu@gmail.com"));
		aboutPanel.add(okButtonAbout);
		
		Scanner input;
		try {
			if(file.exists()==false){
				file.createNewFile();
			}
			input = new Scanner(new FileReader(file));
			while(input.hasNextLine()){
				listModel.addElement(input.nextLine());
			}
			input.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		wordList.setModel(listModel);
		wordList.setBorder(BorderFactory.createEtchedBorder());
		
		leftPanel.setBorder(BorderFactory.createTitledBorder("English To Bengali"));
		setLayout(new BorderLayout());
		add(leftPanel,BorderLayout.WEST);
		add(rightPanel,BorderLayout.EAST);
		add(new JScrollPane(rightPanel),BorderLayout.CENTER);
		
		leftPanel.add(innerLeft);
				
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		innerLeft.add(searchLabel,c);
		
		c.gridx++;
		innerLeft.add(searchBox,c);
		
		c.gridx++;
		innerLeft.add(button,c);
		
		c.gridy++;
		c.gridx = 1;
		c.anchor = GridBagConstraints.LINE_START;
		innerLeft.add(addToFav,c);
		
		c.gridy++;
		c.gridx = 0;
		innerLeft.add(favLabel,c);
		
		c.gridy++;
		c.gridx=1;
		c.anchor = GridBagConstraints.LINE_START;
		JScrollPane js = new JScrollPane(wordList);
		js.setPreferredSize(new Dimension(110,250));
		innerLeft.add(js,c);
		
		c.gridy++;
		c.anchor = GridBagConstraints.LINE_START;
		innerLeft.add(clearButton,c);
				
		rightPanel.add(imgLabel);
		
		button.addActionListener(this);
		addFavItem.addActionListener(this);
		addToFav.addActionListener(this);
		clearFavItem.addActionListener(this);
		clearButton.addActionListener(this);
		exitDictItem.addActionListener(this);
		aboutItem.addActionListener(this);
		okButtonAbout.addActionListener(this);
		
		wordList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()){
		            String selected = wordList.getSelectedValue().toString().toLowerCase();
		            //selected = Character.toUpperCase(selected.charAt(0)) + selected.substring(1);
		            //imgLabel.setIcon(new ImageIcon("data/"+selected));
		            URL imgUrl = getClass().getResource("/data/"+selected);
		            if(imgUrl != null){
		            	imgLabel.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/data/"+selected))));
		            }    
		            
		        }
			}
		});
		ImageIcon img = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icon/icon.png")));
		setIconImage(img.getImage());
		setSize(640,480);
		rootPane.setDefaultButton(button);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setVisible(true);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == button){
			String word = searchBox.getText().toLowerCase();
			//String word = Character.toUpperCase(searchBox.getText().charAt(0)) + searchBox.getText().substring(1);
			//imgLabel.setIcon(new ImageIcon("/data/"+word));
			URL imgUrl = getClass().getResource("/data/"+word);
			if(imgUrl != null){
				imgLabel.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/data/"+word))));
			}
			
			//imgLabel.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("data/"+word))));
			//Toolkit.getDefaultToolkit().getImage("");
		}
		else if(e.getSource() == addToFav || e.getSource() == addFavItem){
			String favWord = searchBox.getText();
			try {
				if (file.exists() == false) {
					file.createNewFile();
				}
				PrintWriter pw = new PrintWriter(new FileWriter(file, true));
				if(!favWord.equals("")){
					pw.println(favWord);
					listModel.addElement(favWord);
				}
				pw.close();
			} catch (Exception e2) {
				System.err.println(e2);
			}
		}
		else if(e.getSource() == clearButton || e.getSource() == clearFavItem){
			file.delete();
			JOptionPane.showMessageDialog(rootPane, "Cleared. Please Restart BanglaDictX.");
		}
		else if(e.getSource() == aboutItem){
			aboutDialog.setVisible(true);
		}
		else if(e.getSource() == okButtonAbout){
			aboutDialog.dispose();
		}
		else if(e.getSource() == exitDictItem){
			dispose();
		}
		
		
	}
}
